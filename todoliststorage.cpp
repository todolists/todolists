#include "todoliststorage.h"

#include "todolist.h"
#include "todoitem.h"

#include <QSqlDatabase>
#include <QTimer>
#include <QSqlQuery>
#include <QSqlDriver>
#include <QSqlError>
#include <QDir>
#include <QtDebug>
#include <QVariant>
#include <QSqlField>

TodoListStorage::TodoListStorage(QString filePath)
        : filePath(filePath)
{
}

bool TodoListStorage::open()
{
        //qDebug() << "SQLite driver available =" << QSqlDatabase::isDriverAvailable("QSQLITE");
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    qDebug() << "driver caps:\n\tquerysize = " << db.driver()->hasFeature(QSqlDriver::QuerySize)
            << "\n\tlastinsertid = " << db.driver()->hasFeature(QSqlDriver::LastInsertId)
            << "\n\tprepared queries = " << db.driver()->hasFeature(QSqlDriver::PreparedQueries)
            << "\n\tnamed placeholders = " <<  db.driver()->hasFeature(QSqlDriver::NamedPlaceholders);
    qDebug() << "used db = " << filePath;
    db.setDatabaseName(this->filePath);
    if (!db.open()) {
        qDebug() << "failed opening db (TODO handle)"  << db.lastError().text();
        return false;
    }
    if (!db.tables().contains("todos")) {
        qDebug() << "db does not contain todos => create table";
        QSqlQuery query("CREATE TABLE todos (ID INTEGER PRIMARY KEY AUTOINCREMENT, list INTEGER NOT NULL, \
                         labels TEXT NULL, todo TEXT, state INTEGER NOT NULL, priority INTEGER NOT NULL, \
                         created INTEGER NOT NULL, modified INTEGER NOT NULL)");
        //if ( query.lastError().type() != QSqlError::NoError)
        if ( !query.isActive()) {
            qDebug() << "error creating table" << query.lastError().text();
            return false;
        } else {
            qDebug() << "table successfully created";
        }
    }
    if (!db.tables().contains("todolistmetadata")) {
        qDebug() << "db does not contain todolistmetadata => create table";
        QSqlQuery query("CREATE TABLE todolistmetadata (ID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, \
                         displayedColumns INTEGER, headerState BLOB, created INTEGER NOT NULL)");
        //if ( query.lastError().type() != QSqlError::NoError)
        if ( !query.isActive()) {
            qDebug() << "error creating table" << query.lastError().text();
            return false;
        } else {
            qDebug() << "table successfully created";
        }
    }
    return true;
}

bool TodoListStorage::close()
{
    return true;
}

QList<TodoList*>* TodoListStorage::getLists()
{
    qDebug() << "TodoListStorage::getLists";
    QString queryStr = QString("SELECT ID, name, displayedColumns, headerState, datetime(created, 'localtime') FROM todolistmetadata");
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error fetching lists metadata" << query.lastError().text();
        return 0;
    }

    QList<TodoList*>* lists = new QList<TodoList*>;
    while (query.isActive() && query.next()) {
        TodoList* list = new TodoList(query.value(0).toLongLong());
        list->name = query.value(1).toString();
        list->displayedColumns = query.value(2).toInt();
        list->headerState = query.value(3).toByteArray();
        list->created = query.value(4).toDateTime();
        lists->append(list);
    }
    return lists;
}

/*
bool TodoListStorage::initList(TodoList* list)
{
    qDebug() << "TodoListStorage::initList" << list->id();
    QString queryStr = QString("SELECT ID, name, displayedColumns FROM todolistmetadata WHERE ID = VALUES %1").arg(list->id());
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error initializing list" << query.lastError().text();
        return false;
    }

    if (query.isActive() && query.next()) {
        list->name = query.value(0).toString();
        list->displayedColumns = query.value(2).toInt();
    }
    return true;
}
*/

bool TodoListStorage::loadTodos(TodoList* list)
{
    qDebug() << "TodoListStorage::loadTodos" << list->name << "ID =" << list->id();
    QSqlQuery query(QString("SELECT ID, todo, state, labels, datetime(created, 'localtime'), datetime(modified, 'localtime'), priority "
                            "FROM todos where list = '%1' "
                            "ORDER BY priority").arg(list->id()));
    //qDebug() << "querystr" << query.executedQuery();
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error loading todos" << query.lastError().text();
        return false;
    }

    while (query.isActive() && query.next()) {
        TodoItem* item = new TodoItem(list,
                                      query.value(0).toLongLong(),
                                      query.value(1).toString(),
                                      query.value(3).toString().split(','),
                                      query.value(6).toInt(),
                                      query.value(4).toDateTime(),
                                      query.value(5).toDateTime(),
                                      (TodoItem::State)query.value(2).toInt());
        list->items->append(item);

    }
    return true;
}

bool TodoListStorage::unloadTodos(TodoList* list)
{
    //todo implement
    Q_UNUSED(list)
    return false;
}

TodoList* TodoListStorage::createList(QString name)
{
    qDebug() << "TodoListStorage::createList" << name;
    QSqlQuery query(QString("INSERT INTO todolistmetadata (name, displayedColumns, created) VALUES ('%1', %2, datetime('now'))").arg(name).arg(TodoList::Default));
    //qDebug() << "querystr" << query.executedQuery();
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error creating list" << query.lastError().text();
        return NULL;
    }
    qlonglong newListID = query.lastInsertId().toLongLong();
    qDebug() << "last insert id = " << newListID;
    TodoList* todoList = new TodoList(newListID, name);
    todoList->setCreated(QDateTime::currentDateTime());
    return todoList;
}

bool TodoListStorage::deleteList(TodoList* list)
{
    qDebug() << "TodoListStorage::deleteList" << list->name;
    QString queryStr = QString("DELETE FROM todos WHERE list = %1").arg(list->id());
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error deleting todos" << query.lastError().text();
        return false;
    }

    queryStr = QString("DELETE FROM todolistmetadata WHERE ID = %1").arg(list->id());
    query = QSqlQuery(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error deleting todolist" << query.lastError().text();
        return false;
    }

    return true;
}

bool TodoListStorage::saveListMetadata(TodoList* list)
{
    qDebug() << "TodoListStorage::saveListMetadata" << list->name;
    QSqlQuery query;
    //when storing qbytearrays, it's easier to use parameter binding
    //(although the drivers advertises named placeholders are not supported?!)
    query.prepare("UPDATE todolistmetadata "
                  "SET name = :name, displayedColumns = :displayedColumns, headerState = :headerState "
                  "WHERE ID = :id");
    query.bindValue(":id", list->id());
    query.bindValue(":name", escapeDBString(list->getName()));
    query.bindValue(":displayedColumns", list->displayedColumns);
    query.bindValue(":headerState", list->headerState);
    query.exec();
    qDebug() << "querystr" << query.executedQuery();
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error saving metadata" << query.lastError().text();
        return false;
    }

    return true;
}

TodoItem* TodoListStorage::insertTodo(TodoList* list, QString text)
{
    int prio = list->getMaxOrder()+1;
    QString queryStr = QString("INSERT INTO todos (list, todo, state, created, modified, priority) "
                               "VALUES (%1, '%2', %3, datetime('now'), datetime('now'), %4)")
                       .arg(list->id())
                       .arg(escapeDBString(text))
                       .arg(TodoItem::New)
                       .arg(prio);
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error inserting todo" << query.lastError().text();
        return false;
    }
    qDebug() << "last insert id = " << query.lastInsertId();
    return new TodoItem(list, query.lastInsertId().toLongLong(), text, QStringList(), prio);
}

bool TodoListStorage::updateTodo(TodoItem* item)
{
    QString queryStr = QString("UPDATE todos SET todo = '%2', state = %3, modified = datetime('%4'), priority = %5 "
                               "WHERE ID = %1")
                       .arg(item->id())
                       .arg(escapeDBString(item->getText()))
                       .arg(item->getState())
                       .arg(QDateTime::currentDateTime().toString(Qt::ISODate))
                       .arg(item->getPriority());
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error updating todo" << query.lastError().text();
        return false;
    }
    return true;
}

bool TodoListStorage::deleteTodo(TodoItem* item)
{
    QString queryStr = QString("DELETE FROM todos WHERE ID = %1").arg(item->id());
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error deleting todo" << query.lastError().text();
        return false;
    }

    return true;
}

bool TodoListStorage::moveTodo(TodoItem *item, int todoListID)
{
    QString queryStr = QString("UPDATE todos SET list = %2 WHERE ID = %1")
                       .arg(item->id())
                       .arg(todoListID);
    QSqlQuery query(queryStr);
    qDebug() << "querystr" << queryStr;
    qDebug() << "query success = " << query.isActive();
    if (!query.isActive()) {
        qDebug() << "error updating todo" << query.lastError().text();
        return false;
    }
    return true;
}

QSqlField stringField("str", QVariant::String);

QString TodoListStorage::escapeDBString(QString str)
{
    return str.replace("'", "''");
}

QString TodoListStorage::unescapeDBString(QString str)
{
    return str.replace("''", "'");
}

