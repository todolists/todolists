#include <QtWidgets/QApplication>

#include "mainwindow.h"

#include <QDir>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("todolists");

    QApplication app(argc, argv);
    QString dbPath;
    if (argc > 1) {
        dbPath = app.arguments().at(1);
    } else {
        dbPath = QDir::homePath() + "/todolists.db";
    }
    MainWindow win(dbPath);
    win.show();
    win.init();
    return app.exec();
}
