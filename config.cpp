#include "config.h"

#include <QSettings>
#include <QtDebug>
#include <QDir>

Config* config;

Config::Config()
{
        read();
}

Config::~Config()
{
        write();
}

void Config::read()
{
        qDebug() << "Config::read()";
        QSettings settings;

        checkVersionURL = settings.value("general/checkVersionURL", "todolists/latest_version.txt").toString();
        checkForUpdates = settings.value("general/checkForUpdates", false).toBool();
        windowGeom = settings.value("general/windowGeometry").toByteArray();
        splitterState = settings.value("general/splitter").toByteArray();

        lastListIndex = settings.value("general/lastListIndex", -1).toInt();
        dateTimeFormat = settings.value("general/dateTimeFormat", "yyyy/MM/dd hh:mm").toString();

        lastExportDir = settings.value("export/lastDir", QDir::homePath()).toString();
}

void Config::write()
{
        qDebug() << "Config::write()";

        QSettings settings;
        settings.setValue("general/version", "0.1");
        settings.setValue("general/checkVersionURL", checkVersionURL);
        settings.setValue("general/checkForUpdates", checkForUpdates);


        settings.setValue("general/lastListIndex", lastListIndex);

        settings.setValue("export/lastDir", lastExportDir);
}
