#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "todoliststorage.h"
#include "config.h"
#include "aboutdialog.h"

#include <QtWidgets/QMainWindow>

class TodoList;
class QCloseEvent;
class QShortcut;
class MoveItemDialog;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QString dbPath);
    ~MainWindow();
    bool init();

public slots:
    void onCurrentIndexChanged(int);
    void onAddTodoButton_clicked();
    void onAddListButton_clicked();
    void onRemoveListButton_clicked();
    void onListPropsButton_clicked();
    void onExportButton_clicked();
    void onHelpButton_clicked();
    void onNextListShortcut_activated();
    void onPrevListShortcut_activated();
    void onMoveItemShortcut_activated();

protected:
    void keyPressEvent ( QKeyEvent * event );
    void closeEvent(QCloseEvent* event);
    bool eventFilter(QObject *obj, QEvent *ev);

private:
    void initGUI();
    void applyListSettings(TodoList* list);

    Ui::MainWindow *ui;
    AboutDialog *aboutDialog;
    MoveItemDialog* moveItemDialog;
    TodoListStorage* storage;
//    TodosListModel* listmodel;
    TodoList* currentList;

    QShortcut* nextList;
    QShortcut* prevList;
    QShortcut* moveItem;

};
/*
class KeyPressFilter : public QObject
{
    Q_OBJECT

    KeyPressFilter(MainWindow* mainWindow);
 protected:
     bool eventFilter(QObject *obj, QEvent *event);
};
*/
#endif // MAINWINDOW_H
