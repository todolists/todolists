#include "todoitem.h"

#include "todolist.h"
#include <QtDebug>

TodoItem::TodoItem(TodoList* list, qlonglong ID, QString todoText, QStringList labels, int priority, QDateTime created, QDateTime modified, State initialState)
        : QObject(list), ID(ID), list(list), text(todoText), labels(labels), priority(priority), state(initialState),
        created(created), modified(modified)
{
}

TodoItem::TodoItem(const TodoItem &item)
    : QObject(item.list), ID(item.ID), list(item.list), text(item.text), labels(item.labels), priority(item.priority),
    state(item.state), created(item.created), modified(item.modified)
{

}

bool TodoItem::setText(QString text)
{
    this->text = text;
    this->modified = QDateTime::currentDateTime();
    return list->updateTodo(this);
}

QString TodoItem::getText()
{
    return text;
}

QDebug operator<<(QDebug dbg, const TodoItem &item)
{
    dbg.nospace() << item.ID << " " << item.priority << item.text;

    return dbg.space();
}
