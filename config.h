#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include <QByteArray>

class Config
{
public:
    Config();
    virtual ~Config();

private:
    void read();
    void write();

public: // members
    QString checkVersionURL;
    QString checkForUpdates;
    QByteArray windowGeom;
    QByteArray splitterState;

    int lastListIndex;
    QString lastExportDir;
    QString dateTimeFormat;
};

extern Config* config;

#endif // CONFIG_H
