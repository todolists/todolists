#ifndef TODOITEMVIEWDELEGATE_H
#define TODOITEMVIEWDELEGATE_H

#include <QStyledItemDelegate>

class TodoItemViewDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit TodoItemViewDelegate(QObject *parent = 0);

    void paint(QPainter *painter,
               const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &index) const;
    QString displayText ( const QVariant & value, const QLocale & locale ) const;
signals:

public slots:

private:
    const QString dateTimeFormat;
};

#endif // TODOITEMVIEWDELEGATE_H
