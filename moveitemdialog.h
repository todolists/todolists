#ifndef MOVEITEMDIALOG_H
#define MOVEITEMDIALOG_H

#include <QDialog>

class TodoListStorage;
class TodoList;

namespace Ui {
    class MoveItemDialog;
}

class MoveItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MoveItemDialog(TodoListStorage* storage, QWidget *parent = 0);
    ~MoveItemDialog();

    void refreshLists();
    TodoList* getSelectedList();

private:
    Ui::MoveItemDialog *ui;
    TodoListStorage* storage;
};

#endif // MOVEITEMDIALOG_H
