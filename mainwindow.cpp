#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "todoslistmodel.h"
#include "todolist.h"
#include "listpropsdialog.h"
#include "todoitemviewdelegate.h"
#include "moveitemdialog.h"

#include <QtDebug>
#include <QtCore>
#include <QtWidgets>

MainWindow::MainWindow(QString dbPath)
    : QMainWindow(NULL), ui(new Ui::MainWindow), storage(new TodoListStorage(dbPath)), currentList(0),
    moveItemDialog(0), aboutDialog(0)
{
    config = new Config();
    ui->setupUi(this);
    initGUI();
}

MainWindow::~MainWindow()
{
    config->lastListIndex = ui->listCB->currentIndex();
    delete ui;
    delete storage;
    delete config;
}

void MainWindow::initGUI()
{
    connect(ui->listCB, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChanged(int)));
    connect(ui->addTodoButton, SIGNAL(clicked()), this, SLOT(onAddTodoButton_clicked()));
    connect(ui->todoLE, SIGNAL(returnPressed()), ui->addTodoButton, SLOT(click()));
    connect(ui->addListButton, SIGNAL(clicked()), this, SLOT(onAddListButton_clicked()));
    connect(ui->removeListButton, SIGNAL(clicked()), this, SLOT(onRemoveListButton_clicked()));
    connect(ui->listPropsButton, SIGNAL(clicked()), this, SLOT(onListPropsButton_clicked()));
    connect(ui->exportButton, SIGNAL(clicked()), this, SLOT(onExportButton_clicked()));
    connect(ui->helpButton, SIGNAL(clicked(bool)), this, SLOT(onHelpButton_clicked()));

    restoreGeometry(config->windowGeom);
    ui->splitter->restoreState(config->splitterState);

    nextList = new QShortcut(QKeySequence(tr("ctrl+down")), this);
    prevList = new QShortcut(QKeySequence(tr("ctrl+up")), this);
    connect(nextList, SIGNAL(activated()), this, SLOT(onNextListShortcut_activated()));
    connect(prevList, SIGNAL(activated()), this, SLOT(onPrevListShortcut_activated()));

    moveItem = new QShortcut(QKeySequence(tr("ctrl+m")), this);
    connect(moveItem, SIGNAL(activated()), this, SLOT(onMoveItemShortcut_activated()));

    ui->todoListView->installEventFilter(this);
    ui->todoListView->setItemDelegate(new TodoItemViewDelegate(ui->todoListView));
    ui->todoListView->verticalHeader()->setDefaultSectionSize(ui->todoListView->verticalHeader()->minimumSectionSize());
    ui->todoListView->horizontalHeader()->resizeSection(0, ui->todoListView->verticalHeader()->minimumSectionSize());
}

void MainWindow::keyPressEvent ( QKeyEvent * event )
{
/*
    qDebug() << "key = " << event->key();
    if (event->key() == Qt::Key_Delete && !ui->todoListView->selectionModel()->selection().isEmpty()) {
        int row = ui->todoListView->selectionModel()->selectedIndexes().takeAt(0).row();
        qDebug() << "del row " << row;
        if (currentList->removeRow(row)) {
            if (row >= currentList->rowCount(QModelIndex())) {
                row--;
            }
            ui->todoListView->selectionModel()->setCurrentIndex(currentList->index(row,0),
                QItemSelectionModel::ClearAndSelect);
        }
//        QItemSelection selection = ui->todoListView->selectionModel()->selection();
//        selection.takeAt(
    } else */ QMainWindow::keyPressEvent(event);

}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == ui->todoListView) {
        if (event->type() == QEvent::KeyPress) {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
//            qDebug() << "key = " << keyEvent->key();
            if (keyEvent->key() == Qt::Key_Delete && !ui->todoListView->selectionModel()->selection().isEmpty()) {
                QSortFilterProxyModel* proxyModel = currentList->getSortModel();
                QModelIndex proxyIndex = ui->todoListView->selectionModel()->selectedIndexes().takeAt(0);
                int proxyRow = proxyIndex.row();
                int row = proxyModel->mapToSource(proxyIndex).row();
                qDebug() << "del row " << row << "(source)" << proxyRow << "(proxy)";
                if (proxyModel->removeRow(proxyRow)) {
                    if (proxyRow >= currentList->rowCount(QModelIndex())) {
                        proxyRow--;
                    }
                    qDebug() << "selecting proxy row" << proxyRow;
                    ui->todoListView->selectRow(proxyRow);
                }
                return true;
            } else if (keyEvent->key() == Qt::Key_Up && (keyEvent->modifiers().testFlag(Qt::ShiftModifier))
                        && !ui->todoListView->selectionModel()->selection().isEmpty()) {
                QSortFilterProxyModel* proxyModel = currentList->getSortModel();
                QModelIndex proxyIndex = ui->todoListView->selectionModel()->selectedIndexes().takeAt(0);
                if (proxyIndex.row() == 0) return true;
                TodoItem* other = proxyModel->data(proxyModel->index(proxyIndex.row()-1,0), Qt::UserRole).value<TodoItem*>();
                qDebug() << "other = " << *other;
                currentList->swapOrder(other, proxyModel->data(proxyIndex, Qt::UserRole).value<TodoItem*>());
                return true;
            } else if (keyEvent->key() == Qt::Key_Down && (keyEvent->modifiers().testFlag(Qt::ShiftModifier))
                        && !ui->todoListView->selectionModel()->selection().isEmpty()) {
                QSortFilterProxyModel* proxyModel = currentList->getSortModel();
                QModelIndex proxyIndex = ui->todoListView->selectionModel()->selectedIndexes().takeAt(0);
                if (proxyIndex.row() == proxyModel->rowCount() -1) return true;
                TodoItem* other = proxyModel->data(proxyModel->index(proxyIndex.row()+1,0), Qt::UserRole).value<TodoItem*>();
                qDebug() << "other = " << *other;
                currentList->swapOrder(other, proxyModel->data(proxyIndex, Qt::UserRole).value<TodoItem*>());
                return true;
            } else return false;
        } else return false;
    } else return QMainWindow::eventFilter(obj, event);
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    qDebug() << "MainWindow::closeEvent";
    QSettings settings;
    settings.setValue("general/windowGeometry", saveGeometry());
    settings.setValue("general/splitter", ui->splitter->saveState());
    QWidget::closeEvent(event);
}

bool MainWindow::init()
{
    if (!storage->open()) return false;

    QList<TodoList*>* lists = storage->getLists();
    if (lists == 0) {
        ui->listCB->addItem("error loading lists metadata");
        return false;
    }
    if (lists->size() == 0) {
        qDebug() << "empty db, adding default todolist";
        ui->listCB->addItem("default", QVariant::fromValue(storage->createList("default")));
    } else {
        foreach (TodoList* list, *lists) {
            ui->listCB->addItem(list->getName(), QVariant::fromValue(list));
        }
    }
    if ((config->lastListIndex == -1) || (config->lastListIndex >= lists->size())) {
        config->lastListIndex = 0;
    }
    ui->listCB->setCurrentIndex(config->lastListIndex);

    return true;
}

void MainWindow::onCurrentIndexChanged(int index)
{
    qDebug() << "MainWindow::onCurrentIndexChanged" << index;
    currentList = ui->listCB->itemData(index).value<TodoList*>();
    if (!currentList->load()) {
        currentList->insertTodo("failed to fetch todos");
    }
    ui->todoListView->setModel(currentList->getSortModel());
    applyListSettings(currentList);
}

void MainWindow::onAddTodoButton_clicked()
{
    QModelIndex index = currentList->insertTodo(ui->todoLE->text());
    qDebug() << "source index" << index;
    if (index.isValid()) {
        QModelIndex proxyIndex = currentList->getSortModel()->mapFromSource(index);
        qDebug() << "proxy index" << proxyIndex;
        ui->todoListView->selectionModel()->select(proxyIndex, QItemSelectionModel::ClearAndSelect);
        ui->todoLE->setText("");
    }
}

void MainWindow::onAddListButton_clicked()
{
    bool ok;
    QString listname = QInputDialog::getText(this, tr("Create new list"),
                                      tr("List name:"), QLineEdit::Normal,
                                      tr("new list", "default value when creating new list"), &ok);
    if (ok && !listname.isEmpty()) {
        TodoList* newList = storage->createList(listname);
        if (newList == 0) {
            ui->listCB->addItem("error creating list");
            return;
        }
        ui->listCB->addItem(listname, QVariant::fromValue(newList));
        ui->listCB->setCurrentIndex(ui->listCB->count()-1);
    }

}

void MainWindow::onRemoveListButton_clicked()
{
    if (QMessageBox::Yes != QMessageBox::question(this, tr("Delete list"),
                          tr("Delete list \"%1\".\nAre you sure?").arg(currentList->getName()),
                          QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes))
        return;
    if (storage->deleteList(currentList)) {
        ui->listCB->removeItem(ui->listCB->currentIndex());
    } else {
        qDebug() << "failed to delete list";
    }
}

void MainWindow::onListPropsButton_clicked()
{
    ListPropsDialog dialog(this, currentList);
    if (dialog.exec() == QDialog::Accepted) {
        currentList->setName(dialog.ui->listNameLE->text());
        TodoList::ShowColumns cols = (dialog.ui->showColStatusCB->isChecked() ? TodoList::State : TodoList::None)
                           | (dialog.ui->showColPrioCB->isChecked() ? TodoList::Priority : TodoList::None)
                           | (dialog.ui->showColNameCB->isChecked() ? TodoList::Name : TodoList::None)
                           | (dialog.ui->showColCreatedCB->isChecked() ? TodoList::CreationTimestamp : TodoList::None)
                           | (dialog.ui->showColModifiedCB->isChecked() ? TodoList::ModifiedTimestamp : TodoList::None);
        currentList->setDisplayedColumns(cols);
        if (dialog.ui->saveHeaderStateCB->isChecked()) {
            currentList->setHeaderState(ui->todoListView->horizontalHeader()->saveState());
        }
        if (dialog.ui->resetHeaderStateCB->isChecked()) {
            currentList->setHeaderState(QByteArray());
        }
        storage->saveListMetadata(currentList);
        applyListSettings(currentList);
    }
}

void MainWindow::onExportButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Export todolist"),
                            config->lastExportDir + "/" + currentList->getName() + ".txt",
                            tr("Text files (*.txt)"));
    if (fileName.isNull()) return;
    qDebug() << "filename = " << fileName;
    config->lastExportDir = QFileInfo(fileName).absolutePath();
    currentList->exportToFile(fileName);
}

void MainWindow::onHelpButton_clicked()
{
    if (aboutDialog == 0) {
        aboutDialog = new AboutDialog(this);
        aboutDialog->setVersion(qApp->applicationVersion());
        aboutDialog->adjustSize();
    }
    aboutDialog->exec();
}

void MainWindow::onNextListShortcut_activated()
{
    if (ui->listCB->currentIndex() < ui->listCB->count() -1) {
        ui->listCB->setCurrentIndex(ui->listCB->currentIndex() + 1);
    }
}

void MainWindow::onPrevListShortcut_activated()
{
    if (ui->listCB->currentIndex() > 0) {
        ui->listCB->setCurrentIndex(ui->listCB->currentIndex() - 1);
    }
}

void MainWindow::applyListSettings(TodoList *list)
{
    qDebug() << "MainWindow::restoreListSettings";
    ui->listCB->setItemText(ui->listCB->currentIndex(), list->getName());
    if (!ui->todoListView->horizontalHeader()->restoreState(list->getHeaderState())) {
        qDebug() << "invalid header state, resetting to current and saving to disk";
        list->setHeaderState(ui->todoListView->horizontalHeader()->saveState());
        storage->saveListMetadata(list);
    }
    TodoList::ShowColumns cols = list->getDisplayedColumns();
    ui->todoListView->setColumnHidden(0, !cols.testFlag(TodoList::State));
    ui->todoListView->setColumnHidden(1, !cols.testFlag(TodoList::Priority));
    ui->todoListView->setColumnHidden(2, !cols.testFlag(TodoList::Name));
    ui->todoListView->setColumnHidden(3, !cols.testFlag(TodoList::CreationTimestamp));
    ui->todoListView->setColumnHidden(4, !cols.testFlag(TodoList::ModifiedTimestamp));
}

void MainWindow::onMoveItemShortcut_activated()
{
    QSortFilterProxyModel* proxyModel = currentList->getSortModel();
    if (ui->todoListView->selectionModel()->selectedIndexes().size() == 0) return;
    QModelIndex proxyIndex = ui->todoListView->selectionModel()->selectedIndexes().takeAt(0);

    if (moveItemDialog == 0) {
        moveItemDialog = new MoveItemDialog(storage, this);
    }
    moveItemDialog->refreshLists();
    if (moveItemDialog->exec() == QDialog::Accepted) {
        TodoItem* item = proxyModel->data(proxyIndex, Qt::UserRole).value<TodoItem*>();
        currentList->moveTodo(item, moveItemDialog->getSelectedList());
    }
}
