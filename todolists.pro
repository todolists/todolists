# -------------------------------------------------
# Project created by QtCreator 2009-08-14T19:07:08
# -------------------------------------------------
QT += sql widgets
TARGET = todolists
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    todoliststorage.cpp \
    todolist.cpp \
    todoitem.cpp \
    config.cpp \
    listpropsdialog.cpp \
    todoitemviewdelegate.cpp \
    prioritysortfilterproxymodel.cpp \
    moveitemdialog.cpp \
    aboutdialog.cpp
HEADERS += mainwindow.h \
    todoliststorage.h \
    todolist.h \
    todoitem.h \
    config.h \
    listpropsdialog.h \
    todoitemviewdelegate.h \
    prioritysortfilterproxymodel.h \
    moveitemdialog.h \
    aboutdialog.h
FORMS += mainwindow.ui \
    listpropsdialog.ui \
    moveitemdialog.ui \
    aboutdialog.ui
RESOURCES += todolists.qrc
RC_FILE = todolists.rc
OTHER_FILES += roadmap.txt
