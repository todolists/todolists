#ifndef TODOLISTSTORAGE_H
#define TODOLISTSTORAGE_H

#include <QString>
#include <QStringList>
#include <QHash>
#include <QObject>
#include <QDir>

class TodoList;
class TodoItem;

class TodoListStorage
{
public:
    TodoListStorage(QString filePath = QDir::homePath()+"/todolists.db");

    bool open();
    bool close();

    QList<TodoList*>* getLists();
//    bool initList(TodoList* list);
    bool loadTodos(TodoList* list);
    bool unloadTodos(TodoList* list);

    TodoList* createList(QString name);
    bool deleteList(TodoList* list);
    bool saveListMetadata(TodoList* list);

    TodoItem* insertTodo(TodoList* list, QString text);
    bool updateTodo(TodoItem* item);
    bool deleteTodo(TodoItem* item);
    bool moveTodo(TodoItem* item, int newListID);

private:
    QString escapeDBString(QString str);
    QString unescapeDBString(QString str);

    QString filePath;
};

#endif // TODOLISTSTORAGE_H
