#include "prioritysortfilterproxymodel.h"

#include "todoitem.h"

#include <QtDebug>

PrioritySortFilterProxyModel::PrioritySortFilterProxyModel(TodoList * parent)
    : QSortFilterProxyModel(parent), todoList(parent)
{
}

bool PrioritySortFilterProxyModel::lessThan ( const QModelIndex & left, const QModelIndex & right ) const
{
    if (left.column() != 1) return QSortFilterProxyModel::lessThan(left, right);
    qDebug() << "PrioritySortFilterProxyModel::lessThan" << left << right;
    TodoItem* leftItem = sourceModel()->data(left, Qt::UserRole).value<TodoItem*>();
    TodoItem* rightItem = sourceModel()->data(right, Qt::UserRole).value<TodoItem*>();

    int leftPrio = leftItem->getPriority();
    int rightPrio = rightItem->getPriority();

    if (leftPrio == 0 && rightPrio != 0) {
        return true;
    }
    if (leftPrio != 0 && rightPrio == 0) {
        return false;
    }

    if (leftPrio == 0 && rightPrio == 0) {
        return leftItem->getText().compare(rightItem->getText()) < 0;
    }

    TodoItem* prioItem;
    int prio = rightItem->getPriority();
    while (prio != 0) {
        prioItem = todoList->getTodo(prio);
        if (prioItem == NULL) {
            qDebug() << "illegal item ID" << prio;
            break;
        }
        if (prioItem == leftItem) return true;
        prio = prioItem->getPriority();
    }
    return false;
}
