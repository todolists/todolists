#ifndef TODOLIST_H
#define TODOLIST_H

#include "todoitem.h"

#include <QList>
#include <QString>
#include <QAbstractListModel>
#include <QMetaType>
#include <QByteArray>

class QSortFilterProxyModel;
class TodoListStorage;
//class TodoListModel;

class TodoList : public QAbstractTableModel
{
    Q_OBJECT
    friend class TodoListStorage;
public:
    enum ShowColumn { None = 0, State = 1, Name = 2, CreationTimestamp = 4, ModifiedTimestamp = 8, Priority = 16, Default = State | Name};
    Q_DECLARE_FLAGS(ShowColumns, ShowColumn);

    TodoList(qlonglong ID = -1, QString name = QString("noname"));
    TodoList(const TodoList& list);
    virtual ~TodoList();

    bool load();
    bool unload();

    QModelIndex insertTodo(QString text);
    bool deleteTodo(TodoItem* todo);
    bool updateTodo(TodoItem* todo);
    bool moveTodo(TodoItem* todo, TodoList* newList);

    TodoItem* getTodo(int id);
    void swapOrder(TodoItem* first, TodoItem* second);
    int getMaxOrder();
    void initOrder();

    qlonglong id() const;
    QString getName() const { return name; }
    void setName(QString name) { this->name = name; }
    void setDisplayedColumns(ShowColumns columns) { this->displayedColumns = columns; }
    ShowColumns getDisplayedColumns() { return (ShowColumns)displayedColumns; }
    QByteArray getHeaderState() const { return headerState; }
    void setHeaderState(QByteArray headerState) { this->headerState = headerState; }
    QSortFilterProxyModel* getSortModel() const { return sortModel; }
    QDateTime getCreated() const { return created; }
    void setCreated(const QDateTime & datetime) { this->created = datetime; }

    void exportToFile(QString filename);

    //--- BEGIN QAbstractTableModel ---
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
//    bool removeRows(int row, int count, const QModelIndex &parent);

    QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    bool removeRows ( int row, int count, const QModelIndex & parent = QModelIndex() );
    //--- END QAbstractTableModel ---
private:
    qlonglong ID;
    QString name;
    QByteArray headerState;
    bool sync;
    int displayedColumns;
    QList<TodoItem*>* items;
    TodoListStorage* storage;
    QSortFilterProxyModel* sortModel;
    QDateTime created;
};
Q_DECLARE_METATYPE(TodoList*)
Q_DECLARE_OPERATORS_FOR_FLAGS(TodoList::ShowColumns);

#endif // TODOLIST_H
