#include "todolist.h"

#include "todoliststorage.h"

#include "prioritysortfilterproxymodel.h"

#include <QtDebug>
#include <QKeyEvent>
#include <QSortFilterProxyModel>

/*
TodoList::TodoList()
: QAbstractListModel(), ID(-1), name("default ctor"), sync(true)
{
}
*/
TodoList::TodoList(qlonglong ID, QString name)
        : QAbstractTableModel(), ID(ID), name(name), sync(true), displayedColumns(Default), items(0)
{
    sortModel = new QSortFilterProxyModel(this);
    sortModel->setDynamicSortFilter(true);
    sortModel->setSourceModel(this);
}

TodoList::TodoList(const TodoList& list)
        : QAbstractTableModel(), ID(list.ID), name(list.name), sync(list.sync), displayedColumns(list.displayedColumns), items(list.items), sortModel(list.sortModel), created(list.created)
{
    sortModel->setSourceModel(this);
}

TodoList::~TodoList()
{
    if (items != 0) delete items;
}

bool TodoList::load()
{
    if (items != 0) {
        qDebug() << "TodoList::load(): items = " << items << " -> going to delete";
        delete items;
    }
    items = new QList<TodoItem*>();
    return storage->loadTodos(this);
}

bool TodoList::unload()
{
    delete items;
    items = 0;
    return true;
}

//@todo override insertRows analog to removeRows
QModelIndex TodoList::insertTodo(QString text)
{
    beginInsertRows(QModelIndex(), items->count(), items->count());
    TodoItem* newItem = 0;
    if (sync) {
        newItem = storage->insertTodo(this, text);
        if (newItem != 0) {
            qDebug() << "appending new item";
            items->append(newItem);
        } else return QModelIndex();
    } else {
        newItem = new TodoItem(this, -1, text, QStringList());
        items->append(newItem);
    }
    QModelIndex index = createIndex(items->count()-1, 0, newItem);
//    qDebug() << "emit datachanged" << index;
//    emit dataChanged(index, index);
    endInsertRows();
    return index;
}

bool TodoList::deleteTodo(TodoItem* todo)
{
    if (sync) {
        return storage->deleteTodo(todo);
    } else {
        return true;
    }
}

bool TodoList::updateTodo(TodoItem* todo)
{
    qDebug() << "TodoList::updateTodo";
    if (sync) {
        return storage->updateTodo(todo);
    } else return true;
}

bool TodoList::moveTodo(TodoItem *todo, TodoList* newList)
{
    qDebug() << "TodoList::moveTodo";
    if (sync) {
        int i = this->items->indexOf(todo);
        qDebug() << "move index " << i;
        bool success = true;
        beginRemoveRows(QModelIndex(), i, i);
        success = storage->moveTodo(items->at(i), newList->ID);
        if (success) {
            items->removeAt(i);
            todo->list = newList;
        }
        endRemoveRows();

        return success;
    } else return true;
}

/**
  not used anymore
  */
TodoItem* TodoList::getTodo(int ID)
{
    //todo optimize
    TodoItem* item;
    foreach (item, *items) {
        if (item->id() == ID) return item;
    }
    qDebug() << "requested item with id" << ID << "not found";
    return 0;
}

/** first becomes second. second becomes first
  */
void TodoList::swapOrder(TodoItem* first, TodoItem* second)
{
    //todo possibly better to accept model indices as argument
    int prioFirst = first->getPriority();
    int prioSecond = second->getPriority();
    if (prioFirst == prioSecond) {
        initOrder();
        prioFirst = first->getPriority();
        prioSecond = second->getPriority();
    }
    setData(index(items->indexOf(first), 1), prioSecond);
    setData(index(items->indexOf(second), 1), prioFirst);
}

/**
  returns highest used priority in this todolist

  current storage backend ensures todo items are loaded sorted by priority
  so item with highest priority is always last
  */
int TodoList::getMaxOrder()
{
    if (items->size() == 0) return 0;
    else return items->at(items->size()-1)->getPriority();
}

/**
  initializes items with default priority
  only happens when operating on migrated data
  */
void TodoList::initOrder()
{
    qDebug() << "TodoList::initOrder()";
    TodoItem* item;
    for (int i = 0;i<items->size();i++) {
        item = items->at(i);
        item->setPriority(i+1);
        updateTodo(item);
    }
}

qlonglong TodoList::id() const
{
    return ID;
}

void TodoList::exportToFile(QString filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
         return;

    QTextStream out(&file);
    out << getName() << endl << "--------------------------" << endl;
    foreach (TodoItem* item, *items) {
        out << "- " << item->getText() << endl;
    }
    file.close();
}

QVariant TodoList::data(const QModelIndex &index, int role) const
{
//    qDebug() << "QAbstractItemdata" << index << role << "col =" << index.column();
    if (!index.isValid()) {
        qDebug() << "invalid index";
        return QVariant();
    }


//    if (role != Qt::DisplayRole && role != Qt::EditRole) return QVariant();
    TodoItem* item = items->at(index.row());
    switch(role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
        switch (index.column()) {
        case 0:
            return item->getState();
        case 1:
            return item->getPriority();
        case 2:
            return item->getText();
        case 3:
            return item->getCreated();
        case 4:
            return item->getModified();
        default:
            return QString("unknown col %1").arg(index.column());
        }
    case Qt::UserRole:
        return QVariant::fromValue<TodoItem*>(item);
    default:
        return QVariant();
    }
}

Qt::ItemFlags TodoList::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant TodoList::headerData ( int section, Qt::Orientation orientation, int role) const
{
    //essential: otherwise no header is shown!
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section){
        case 0: return "State";
        case 1: return "Prio";
        case 2: return "Item";
        case 3: return "Created";
        case 4: return "Modified";
        default: return "unknown column";
        }
    } else {
        return QVariant();
    }
}

int TodoList::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return items->count();
}

int TodoList::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 5;
}

bool TodoList::setData ( const QModelIndex & index, const QVariant & value, int role)
{
    qDebug() << "TodoList::setData";
    if (!index.isValid()) return false;
    if (role != Qt::EditRole)
         return false;

    TodoItem* item = items->at(index.row());
    TodoItem backup(*item);
    switch(index.column()) {
    case 0:
        item->setState((TodoItem::State)value.toInt());
        break;
    case 1:
        item->setPriority(value.toInt());
        break;
    case 2:
        item->setText(value.toString());
        break;
    default:
        qDebug() << "TodoList::setData unknown col" << index.column();
        return false;
    }
    if (!updateTodo(item)) {
        item->setState(backup.getState());
        item->setText(backup.getText());
        item->setPriority(backup.getPriority());
        return false;
    }

    emit dataChanged(index, index);
    return true;
}

bool TodoList::removeRows ( int row, int count, const QModelIndex & parent)
{
    qDebug() << "TodoList::removeRows" << row << count;
    bool success = false;
    beginRemoveRows(parent, row, row+count-1);
    for (int i = row;i<row+count;i++) {
        if ((success = deleteTodo(items->at(i))) == true) items->removeAt(i);
    }
    endRemoveRows();
    return success;
}

