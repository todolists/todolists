#ifndef TODOSLISTMODEL_H
#define TODOSLISTMODEL_H


#include <QAbstractListModel>

class TodoList;

class TodosListModel : public QAbstractListModel
{
    friend class TodoList;

    Q_OBJECT
public:
    TodosListModel(TodoList* todolist);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
/*    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool removeRows(int row, int count, const QModelIndex &parent);

    bool dropMimeData(const QMimeData *data, Qt::DropAction action,
               int row, int column, const QModelIndex &parent);
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    QStringList mimeTypes() const;*/
    int rowCount(const QModelIndex &parent) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    void setTodoList(TodoList* newList);

private:
    TodoList* todolist;
};

#endif // TODOSLISTMODEL_H
