#include "listpropsdialog.h"

#include "todolist.h"

ListPropsDialog::ListPropsDialog(QWidget* parent, TodoList* todoList)
        : QDialog(parent), ui(new Ui::ListPropsDialog)
{
    ui->setupUi(this);
    ui->listNameLE->setText(todoList->getName());
    TodoList::ShowColumns cols = todoList->getDisplayedColumns();
    ui->showColCreatedCB->setChecked(cols.testFlag(TodoList::CreationTimestamp));
    ui->showColModifiedCB->setChecked(cols.testFlag(TodoList::ModifiedTimestamp));
    ui->showColNameCB->setChecked(cols.testFlag(TodoList::Name));
    ui->showColStatusCB->setChecked(cols.testFlag(TodoList::State));
    ui->showColPrioCB->setChecked(cols.testFlag(TodoList::Priority));
    ui->creationDateLabel->setText(todoList->getCreated().toString());
}
