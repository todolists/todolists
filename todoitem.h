#ifndef TODOITEM_H
#define TODOITEM_H

#include <QString>
#include <QStringList>
#include <QMetaType>
#include <QDateTime>
#include <QObject>

class TodoList;

class TodoItem : public QObject
{
    friend class TodoItemViewDelegate;
    friend class TodoList;
    Q_OBJECT

public:
    enum State { New, InProgress, Done };

    TodoItem(TodoList* list, qlonglong ID, QString todoText, QStringList labels, int priority = 0,
             QDateTime created = QDateTime::currentDateTime(), QDateTime modified = QDateTime::currentDateTime(),
             State initialState = New);
    TodoItem(const TodoItem& list);

    bool setText(QString text);
    QString getText();
    inline qlonglong id() const { return ID; }
    inline void setState(State newState) { state = newState; }
    inline State getState() const { return state; }
    inline QDateTime getCreated() const { return created; }
    inline QDateTime getModified() const { return modified; }
    inline int getPriority() const { return priority; }
    inline void setPriority(int prio) { priority = prio; }

    friend QDebug operator<<(QDebug dbg, const TodoItem &item);
private:
    qlonglong ID;
    TodoList* list;
    QString text;
    QStringList labels;
    int priority;
    State state;
    QDateTime created;
    QDateTime modified;
};
Q_DECLARE_METATYPE(TodoItem*)

QDebug operator<<(QDebug dbg, const TodoItem &item);

#endif // TODOITEM_H
