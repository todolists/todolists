#include "todoslistmodel.h"

#include "todolist.h"

#include <QtDebug>

TodosListModel::TodosListModel(TodoList* todolist)
        : todolist(todolist)
{
}

QVariant TodosListModel::data(const QModelIndex &index, int role) const
{
    return todolist->items.at(index.row())->getText();
}

int TodosListModel::rowCount(const QModelIndex &parent) const
{
    return todolist->items.count();
}

bool TodosListModel::setData ( const QModelIndex & index, const QVariant & value, int role)
{
    qDebug() << "TodosListModel::setData";
    if (index.isValid()) return false;
    todolist->items.at(index.row())->setText(value.toString());
    emit dataChanged(index, index);
    return true;
}

void TodosListModel::setTodoList(TodoList* newList)
{
    qDebug() << "TodosListModel::setTodoList";
    todolist = newList;
    emit dataChanged(createIndex(0,0), createIndex(newList->items.count() - 1, 0));
}
