#include "moveitemdialog.h"
#include "ui_moveitemdialog.h"

#include "todolist.h"
#include "todoliststorage.h"

MoveItemDialog::MoveItemDialog(TodoListStorage* storage, QWidget *parent) :
        QDialog(parent),
        ui(new Ui::MoveItemDialog),
        storage(storage)
{
    ui->setupUi(this);
}

MoveItemDialog::~MoveItemDialog()
{
    delete ui;
}

void MoveItemDialog::refreshLists()
{
    int curIndex = ui->todoListsCB->currentIndex();
    if (curIndex == -1) curIndex = 0;
    ui->todoListsCB->clear();
    QList<TodoList*>* lists = storage->getLists();
    if (lists == 0) {
        ui->todoListsCB->addItem("error loading lists metadata");
    } else {
        foreach (TodoList* list, *lists) {
            ui->todoListsCB->addItem(list->getName(), QVariant::fromValue(list));
        }
    }
    ui->todoListsCB->setCurrentIndex(curIndex);
}

TodoList* MoveItemDialog::getSelectedList()
{
    return ui->todoListsCB->itemData(ui->todoListsCB->currentIndex()).value<TodoList*>();
}
