#ifndef PRIORITYSORTFILTERPROXYMODEL_H
#define PRIORITYSORTFILTERPROXYMODEL_H

#include "todolist.h"

#include <QSortFilterProxyModel>

class PrioritySortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    PrioritySortFilterProxyModel(TodoList * parent = 0 );

protected:
    bool lessThan ( const QModelIndex & left, const QModelIndex & right ) const;

private:
    TodoList* todoList;
};

#endif // PRIORITYSORTFILTERPROXYMODEL_H
