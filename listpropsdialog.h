#ifndef LISTPROPSDIALOG_H
#define LISTPROPSDIALOG_H

#include "ui_listpropsdialog.h"

#include <QDialog>

class TodoList;

class ListPropsDialog : public QDialog
{
public:
    ListPropsDialog(QWidget* parent, TodoList* todoList);

    Ui::ListPropsDialog* ui;
};

#endif // LISTPROPSDIALOG_H
