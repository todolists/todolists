#include "todoitemviewdelegate.h"

#include "todoitem.h"
#include "config.h"

#include <QPainter>
#include <QtDebug>
#include <QApplication>
#include <QLabel>

TodoItemViewDelegate::TodoItemViewDelegate(QObject *parent) :
        QStyledItemDelegate(parent), dateTimeFormat(config->dateTimeFormat)
{
}

void TodoItemViewDelegate::paint(QPainter *painter,
                                 const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();
    QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);


    TodoItem* todoItem = index.model()->data(index, Qt::UserRole).value<TodoItem*>();
    Q_ASSERT(todoItem != 0);

    QColor color;
    switch(index.column()) {
    case 0:
        switch(todoItem->getState()) {
        case TodoItem::New:
            color = Qt::red;
            break;
        case TodoItem::InProgress:
            color = Qt::gray;
            break;
        case TodoItem::Done:
            color = Qt::yellow;
            break;
        default:
            color = Qt::black;
        }
//        painter->fillRect(0,0,10,10, color);
        painter->fillRect(opt.rect, color);
        break;
    case 2:
        if (todoItem->getState() == TodoItem::Done) {
            opt.font.setStrikeOut(true);
        }
        QStyledItemDelegate::paint(painter, opt, index);
        break;
    default:
        QStyledItemDelegate::paint(painter, option, index);
    }
    painter->restore();
}

QString TodoItemViewDelegate::displayText ( const QVariant & value, const QLocale & locale ) const {
    switch (value.userType()) {
    case QVariant::DateTime:
        return value.toDateTime().toString(dateTimeFormat);
    default:
        return QStyledItemDelegate::displayText(value, locale);
    }
}

QSize TodoItemViewDelegate::sizeHint(const QStyleOptionViewItem &option,
                                     const QModelIndex &index) const
{
    if (index.column() == 0) {
        return QSize(10, 10);
    } else return QStyledItemDelegate::sizeHint(option, index);
}

QWidget* TodoItemViewDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                      const QModelIndex &index) const
{
    if (index.column() == 0) {
            QLabel* stateSelector = new QLabel(parent);
//            emit commitData(stateSelector);
//            emit closeEditor(stateSelector);
            return stateSelector;
    } else {
        return QStyledItemDelegate::createEditor(parent, option, index);
    }
}

void TodoItemViewDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if (index.column() == 0) {
        //do nothing
    } else QStyledItemDelegate::setEditorData(editor, index);

}

void TodoItemViewDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                  const QModelIndex &index) const
{
    if (index.column() == 0) {
        model->setData(index, (index.data().toInt()+1) % 3);
    } else QStyledItemDelegate::setModelData(editor, model, index);
}
